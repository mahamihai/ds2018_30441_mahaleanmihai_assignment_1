package BLL;

import Model.Flight;
import Model.User;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FlightDAO1 {


    public List<Flight> getAll()
    {

        List<Flight> all=new ArrayList<>();

        Transaction trns = null;
        Session session = HibernateSession.buildSessionFactory1().openSession();
        try {
            trns = session.beginTransaction();
            all= session.createQuery("FROM Flight").list();


            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.flush();
            session.close();
        }
        return all;

    }
public void  deleteRelations(String id)
{
    Transaction trns = null;

    Session session = HibernateSession.buildSessionFactory1().openSession();
    trns = session.beginTransaction();

    SQLQuery query = session.createSQLQuery("delete from flightBoarded where flightId="+id);
    session.getTransaction().commit();

}
    public void deleteFlight(String id)
    {



        Transaction trns = null;
        Session session = HibernateSession.buildSessionFactory1().openSession();
        try {
            trns = session.beginTransaction();


            Flight toBeDeleted=  (Flight)session.get(Flight.class,id);

            for(User aux:(Set<User>)toBeDeleted.getUsers())
            {
                aux.getFlights().remove(toBeDeleted);
            }
            toBeDeleted.setUsers(null);
            session.delete(toBeDeleted);

            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.flush();
            session.close();
        }


    }
    public void addOrModify(Flight incoming)
    {



        Transaction trns = null;
        Session session = HibernateSession.buildSessionFactory1().openSession();
        try {
            trns = session.beginTransaction();
            Flight toBeDeleted=new Flight();
            session.saveOrUpdate(incoming);

            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.flush();
            session.close();
        }


    }
}
