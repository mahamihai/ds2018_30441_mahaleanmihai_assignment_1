package Model;

import java.util.Date;
import java.util.Set;

public class FlightAPI {
    private String flightId;


    private String airplaneType;
    private City departCity;
    private City arrivalCity;

    private Date departDate;
    private Date arrivalDate;

    public String getFlightId() {
        return flightId;
    }

    public FlightAPI() {
    }

    public FlightAPI(String flightId, String airplaneType, City departCity, City arrivalCity, Date departDate, Date arrivalDate) {
        this.flightId = flightId;
        this.airplaneType = airplaneType;
        this.departCity = departCity;
        this.arrivalCity = arrivalCity;
        this.departDate = departDate;
        this.arrivalDate = arrivalDate;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public City getDepartCity() {
        return departCity;
    }

    public void setDepartCity(City departCity) {
        this.departCity = departCity;
    }

    public City getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(City arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Date getDepartDate() {
        return departDate;
    }

    public void setDepartDate(Date departDate) {
        this.departDate = departDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }
}
