package Model;


import java.util.List;
import java.util.Set;


public class User {
    public User() {
    }

    public int getUserId() {
        return userId;
    }
private String password;

    public String getPassword() {
        return password;
    }


    public String getRole() {
        return role;
    }

    public User(String password, int userId, String username, String role) {
        this.password = password;
        this.userId = userId;
        this.username = username;
        this.role = role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public Set<Flight> getFlights() {
        return flights;
    }

    public void setFlights(Set<Flight> flights) {
        this.flights = flights;
    }

    public User(String password, List<Flight> flights, int userId, String username, String role) {
        this.password = password;
        this.userId = userId;
        this.username = username;
        this.role = role;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


    private Set<Flight> flights;

    public User(String password, Set<Flight> flights, int userId, String username, String role) {
        this.password = password;
        this.flights = flights;
        this.userId = userId;
        this.username = username;
        this.role = role;
    }

    private int userId;
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String role;
   // private List<Flight> boardedFlights;






}
