package BLL;

import Model.City;
import Model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class CItyDao {
    public City getCityById(int id)
    {


        Transaction trns = null;
        Session session = HibernateSession.buildSessionFactory1().openSession();
        City found=null;
        try {
            trns = session.beginTransaction();
            found=  (City) session.get(City.class, id);


            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.flush();
            session.close();
        }
        return found;
    }
}
