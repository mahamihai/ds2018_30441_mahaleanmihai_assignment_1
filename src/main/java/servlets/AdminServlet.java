package servlets;

import BLL.CItyDao;
import BLL.FlightDAO1;
import BLL.UserDAO1;
import Model.Flight;
import Model.FlightAPI;
import org.modelmapper.ModelMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/secured/admin")
public class AdminServlet  extends HttpServlet{
    private UserDAO1 _userDao=new UserDAO1();
    private FlightDAO1 _flightDao=new FlightDAO1();
    private CItyDao _cityDao=new CItyDao();


    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        String id=request.getParameter("id");

        this._flightDao.deleteFlight(id);
        doGet(request,response);

       // int flightId=request.getHe
    }
    private String inputForm() {
            String builder="<form  method=POST action=\"/secured/admin\">\n";



            for (Field aux : FlightAPI.class.getDeclaredFields()) {
                if (Modifier.isPrivate(aux.getModifiers())) {
                    aux.setAccessible(true);

                }
                builder +=aux.getName()+"<br>";
               builder+= "  <input type=\"text\" name=\"" +aux.getName()+ "\"/><br>\n" ;

            }
            builder+= " <input type=\"hidden\" name=\"action\" value=\"AddOrModify\"/>";
            builder+="  <input type=\"submit\" value=\"Submit\">\n";
            builder+="</form>\n";
            return builder;

    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
        System.out.println("Here");
        List<Flight> flights = this._flightDao.getAll();
        List<FlightAPI> apiFlights=new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        for(Flight aux:flights)
        {
            apiFlights.add(modelMapper.map(aux,FlightAPI.class));
        }
        String build = "<div>" +
                "<table border=3><tr>";
        for (Field aux : FlightAPI.class.getDeclaredFields()) {
            build += "<th>" + aux.getName() + "</th>";

        }
        build += "<th>DELETE</th>" +
                "</tr>";

        for (int i = 0; i < apiFlights.size(); i++) {
            build += "<tr>";
            FlightAPI flight=apiFlights.get(i);
            for (Field aux : FlightAPI.class.getDeclaredFields()) {
                if (Modifier.isPrivate(aux.getModifiers())) {
                    try {
                        aux.setAccessible(true);

                        System.out.println(aux.get(flight));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    build +="<td> "+aux.get(flight) +"</td>";

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }


            }
            try {
                Field idField=flight.getClass().getDeclaredField("flightId");
                idField.setAccessible(true);

                build+="<td> " +
                        "<form action=/secured/admin"+
               " method=\"POST\">"+
                        " <input type=\"hidden\" name=\"action\" value=\"delete\"/>"+
                        "  <input type=\"hidden\" name=\"id\" value=\""+idField.get(flight) +"\" /> \n"+
                        "<input type=\"submit\" value=\"Submit\">"+
                        "</form>"+
                        "</td>";
                build += "</tr>";
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }

        }
        build+="</table>";
        build+=inputForm();
        build+="<div>";









        response.getWriter().print(build);

    }
    protected void doSaveOrPost(HttpServletRequest request, HttpServletResponse response)
    {

    }

        protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
           if("delete".equals(request.getParameter("action")))
           {
               doDelete(request,response);
           }
           else
           {
               Flight income=new Flight();
              income.setFlightId(request.getParameter("flightId"));
               income.setAirplaneType(request.getParameter("airplaneType"));
               SimpleDateFormat formatter = new SimpleDateFormat("yyyy/mm/dd");

               try {
                   income.setArrivalDate(   formatter.parse(request.getParameter("arrivalDate")));
               } catch (ParseException e) {
                   e.printStackTrace();
               }
               income.setFlightId(request.getParameter("flightId"));
               try {
                   income.setDepartDate(   formatter.parse(request.getParameter("departDate")));
               } catch (ParseException e) {
                   e.printStackTrace();
               }

                   income.setDepartCity(this._cityDao.getCityById(Integer.parseInt(request.getParameter("departCity"))   ));
               income.setArrivalCity(this._cityDao.getCityById(Integer.parseInt(request.getParameter("arrivalCity"))   ));


               this._flightDao.addOrModify(income);
               this.doGet(request,response);
           }


           }


}
