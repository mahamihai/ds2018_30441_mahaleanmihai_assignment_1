package BLL;

import Model.City;
import Model.Flight;
import Model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;

public class UserDAO1 {





public User findUser(String username,String password)
{
    List<User> all=this.getAll();
    for(int i=0;i<all.size();i++)
    {
        User aux=all.get(i);
        if(aux.getPassword().equals(password)&& aux.getUsername().equals(username))
        {
            return aux;
        }
    }
    return null;
}
public List<Flight> getUserFlights(int id)
{

    Transaction trns = null;
    User currentUser=null;
    Session session = HibernateSession.buildSessionFactory1().openSession();
    try {
        trns = session.beginTransaction();
       currentUser= (User) session.createQuery("FROM User where userId=:userId").setParameter("userId",id).list().get(0);


        session.getTransaction().commit();
    } catch (RuntimeException e) {
        if (trns != null) {
            trns.rollback();
        }
        e.printStackTrace();
    } finally {
        //session.flush();
        session.close();
    }
    if(currentUser!=null) {
        return new ArrayList<>(currentUser.getFlights());
    }
    else
    {
        return null;

}
}
    public List<User> getAll()
    {
        List<User> all=new ArrayList<>();

        Transaction trns = null;
        Session session = HibernateSession.buildSessionFactory1().openSession();
        try {
            trns = session.beginTransaction();
            all= session.createQuery("FROM User").list();


            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            //session.flush();
            session.close();
        }
        return all;

    }
}
