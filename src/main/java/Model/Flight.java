package Model;


import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class Flight {
    private String flightId;

    public Flight() {
    }

    public City getDepartCity() {
        return departCity;
    }

    public void setDepartCity(City departCity) {
        this.departCity = departCity;
    }

    public Flight(String flightId, String airplaneType, City departCity) {
        this.flightId = flightId;
        this.airplaneType = airplaneType;
        this.departCity = departCity;
    }

    public String getFlightId() {
        return flightId;
    }

    public Flight(String flightId, String airplaneType) {
        this.flightId = flightId;
        this.airplaneType = airplaneType;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }



    private String airplaneType;
    private City departCity;
    private City arrivalCity;
private Set users;

    public City getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(City arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Flight(String flightId, String airplaneType, City departCity, City arrivalCity, Set users) {
        this.flightId = flightId;
        this.airplaneType = airplaneType;
        this.departCity = departCity;
        this.arrivalCity = arrivalCity;
        this.users = users;
    }

    public Flight(String flightId, String airplaneType, City departCity, Set users) {
        this.flightId = flightId;
        this.airplaneType = airplaneType;
        this.departCity = departCity;
        this.users = users;
    }

    public Set getUsers() {
        return users;
    }

    public Date getDepartDate() {
        return departDate;
    }

    public void setDepartDate(Date departDate) {
        this.departDate = departDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Flight(String flightId, String airplaneType, City departCity, City arrivalCity, Set users, Date departDate, Date arrivalDate) {
        this.flightId = flightId;
        this.airplaneType = airplaneType;
        this.departCity = departCity;
        this.arrivalCity = arrivalCity;
        this.users = users;
        this.departDate = departDate;
        this.arrivalDate = arrivalDate;
    }

    public void setUsers(Set users) {
        this.users = users;
    }
    private Date departDate;
    private Date arrivalDate;


}
