package BLL;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.xml.ws.Response;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class LocalTime {
    public Date getUTC() {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        System.out.println("UTC Time is: " + dateFormat.format(date));
        return date;
    }

    public String callApi(String address) throws IOException {
        StringBuilder result = new StringBuilder();
        URL url = new URL(address);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
     System.out.println(result.toString());
        return result.toString();
    }
    public String getLocalDate(double lat, double lang) throws IOException {

        Date UTC_Time = this.getUTC();
        String response = this.callApi("http://api.timezonedb.com/v2.1/get-time-zone?key=AG024BOGR93B&format=json&by=position&lat="+lat+"&lng="+lang);
        JsonObject jsonObject = new JsonParser().parse(response).getAsJsonObject();

        String localData = jsonObject.get("formatted").getAsString(); //John
       // SimpleDateFormat dt = new SimpleDateFormat("yyyyy-mm-dd hh:mm:ss");
        //System.out.println("The local time for lat:" + lat + " and long:" + lang + " is:" + UTC_Time.toString());
        return localData;
    }

    }

//https://maps.googleapis.com/maps/api/timezone/json?location=39.6034810,-119.6822510&timestamp=1331161200&key=AIzaSyD828Rg3ueUYxC3GdYg7Wx9QGHXCrS64pI
//"http://api.timezonedb.com/v2.1/get-time-zone?key=AG024BOGR93B&format=json&by=position&lat="+lat+"&lng="+lang+"