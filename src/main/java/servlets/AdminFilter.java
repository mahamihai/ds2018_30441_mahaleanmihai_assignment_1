package servlets;

import BLL.UserDAO1;
import Model.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/secured/*")

public class AdminFilter implements Filter{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.print("Working");
    }
private UserDAO1 _userDao=new UserDAO1();
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
String role="";
        HttpSession session=((HttpServletRequest) request).getSession();

try {
     role = session.getAttribute("right").toString();
}
catch(Exception e)
{

}
            if(!role.equals("ADMIN"))
            {
                ((HttpServletResponse)response).sendRedirect("/login");

            }
            else
            {
                chain.doFilter(request, response); // Just continue chain.


            }
        }

    }

