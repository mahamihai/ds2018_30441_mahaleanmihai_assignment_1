package servlets;

import BLL.LocalTime;
import BLL.LoginService;
import BLL.UserDAO1;
import Model.User;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("Here");

        request.getRequestDispatcher("login.jsp").forward(request, response);

    }
    private UserDAO1 _userDao=new UserDAO1();

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("In the post");
        String username = request.getParameter("Username");
        String password = request.getParameter("Password");
        PrintWriter writer=response.getWriter();
        if ((username == null)  || (password==null))
        {
            writer.print("<h1> Wrong credentials</h1>");
        }
        else
        {

          User logged=this._userDao.findUser(username,password);


            if(logged!=null)
            {
                HttpSession session=((HttpServletRequest) request).getSession(true);
                session.setAttribute("Username",username);
                session.setAttribute(
                        "Password",password );
                session.setAttribute(
                                "right",logged.getRole());
                session.setAttribute(
                        "id",logged.getUserId());

                System.out.println("Session set");
                if(logged.getRole().equals("ADMIN"))
                {
                    ((HttpServletResponse )response).sendRedirect("/secured/admin");
                }
                else
                {
                    ((HttpServletResponse )response).sendRedirect("/client");
                }
                writer.print("<div> Logged in with" +
                        username+" "+password
                        +"</div>");

            }
            else {
                writer.print("<div> Please log in" +
                        "</div>");
            }


        }

    }
}