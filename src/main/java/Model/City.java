package Model;

import com.maxmind.geoip.Location;

public class City {
    private int cityId;

    public City(int cityId) {
        this.cityId = cityId;
    }

    public City() {
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLog() {
        return log;
    }

    @Override
    public String toString() {
        return
                name ;

    }

    public void setLog(double log) {
        this.log = log;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City(int cityId, String name, double lat, double log) {
        this.cityId = cityId;
        this.name = name;
        this.lat = lat;
        this.log = log;
    }

    public City(int cityId, double lat, double log) {
        this.cityId = cityId;
        this.lat = lat;
        this.log = log;
    }
private String name;
    private double lat;
  private double log;
}
