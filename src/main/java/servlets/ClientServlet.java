package servlets;

import BLL.FlightDAO1;
import BLL.LocalTime;
import BLL.UserDAO1;
import Model.Flight;

import Model.FlightAPI;
import com.google.gson.Gson;

import org.modelmapper.ModelMapper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/client")
public class ClientServlet extends HttpServlet {
    private FlightDAO1 _flightDao = new FlightDAO1();
    private UserDAO1 _userDao = new UserDAO1();

    private LocalTime _localTime = new LocalTime();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.getWriter().print("<div>");
        String lat=request.getParameter("lat").toString();
        String lang=request.getParameter
                ("long").toString();
        String localData=this._localTime.getLocalDate(Double.parseDouble(lat),Double.parseDouble(lang));
        String message="The local time is "+localData;
        response.getWriter().print("<script>\n" +
                "alert (  ' "+message+"' )"+
                "</script>");


        doGet(request, response);
        response.getWriter().print("</div>");


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = ((HttpServletRequest) request).getSession(true);
        this._localTime.getLocalDate(46.780030, 23.609866);
        List<Flight> flightsRaw =  this._userDao.getUserFlights((int)session.getAttribute("id"));
        List<FlightAPI> flights=new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        for(Flight aux:flightsRaw)
        {
            flights.add(modelMapper.map(aux,FlightAPI.class));
        }
        String build = "<div>" +
                "<table border=3><tr>";
        for (Field aux : FlightAPI.class.getDeclaredFields()) {
            build += "<th>" + aux.getName() + "</th>";

        }

        build += "<th> Local departure time </th>";
        build += "<th> Local arrival time </th>";

        build += "</tr>";

        if (flights != null) {
            for (int i = 0; i < flights.size(); i++) {
                build += "<tr>";
                FlightAPI flight = flights.get(i);
                for (Field aux : FlightAPI.class.getDeclaredFields()) {
                    if (Modifier.isPrivate(aux.getModifiers())) {

                        aux.setAccessible(true);


                    }
                    try {
                        build += "<td> " + aux.get(flight) + "</td>";

                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }


                }
                Gson gson = new Gson();

                build+="<td>" +
                        "<form action=/client"+
                        " method=\"POST\">"+


                "  <input type=\"hidden\" name=\"lat\" value="+flight.getDepartCity().getLat()+     " />  "+
                        "  <input type=\"hidden\" name=\"long\" value="+flight.getDepartCity().getLog()+     " /> "+




        "<input type=\"submit\" value=\"See local time\"/>"+
                "</form>" +
                "</td>";
        build+="<td>" +
                "<form action=/client"+
                " method=\"POST\">"+

                "  <input type=\"hidden\" name=\"lat\" value="+flight.getArrivalCity().getLat()+     " /> "+
                "  <input type=\"hidden\" name=\"long\" value="+flight.getArrivalCity().getLog()+     " /> "+


                "<input type=\"submit\" value=\"See local time\"/>"+
                "</form>" +
                "</td>";


    }

}
        build += "</table>";

                build += "</div>";


                response.getWriter().print(build);
                }

                }
